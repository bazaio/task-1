<div class="support">
    <h1>Обратная связь</h1>
    <h3>У вас есть вопросы по регистрации, личному кабинету, загрузке файлов или содержанию контента? Напишите нам!</h3>

    <form action="/support/send/" method="post" class="validate">
        <div class="form">
            <h4>Информация о вас</h4>
            <table>
                <tr>
                    <td><label for="feedback-first-name">Имя *</label></td>
                    <td><input type="text" name="first_name" maxlength="64" id="feedback-first-name" autocomplete="off" value="<?=$first_name;?>" required></td>
                </tr>
                <tr>
                    <td><label for="feedback-last-name">Фамилия *</label></td>
                    <td><input type="text" name="last_name" maxlength="64" id="feedback-last-name" autocomplete="off" value="<?=$last_name;?>" required></td>
                </tr>
                <tr>
                    <td><label for="feedback-organization">Организация *</label></td>
                    <td><input type="text" name="organization" maxlength="64" id="feedback-organization" autocomplete="off" value="<?=$organization_name;?>" required></td>
                </tr>
                <tr>
                    <td><label for="feedback-phone">Номер телефона *</label></td>
                    <td><input type="tel" name="phone" maxlength="20" id="feedback-phone" autocomplete="off" value="<?=$phone;?>" required></td>
                </tr>
                <tr>
                    <td><label for="feedback-email">E-mail *</label></td>
                    <td><input type="email" name="email" maxlength="64" id="feedback-email" autocomplete="off" value="<?=$email;?>" required></td>
                </tr>
            </table>

            <h4>Ваш вопрос</h4>
            <table>
                <tr>
                    <td><label for="feedback-theme">Тема вопроса *</label></td>
                    <td>
                        <select name="theme" id="feedback-theme" autocomplete="off" required>
                            <option value="Вопрос по работе с сайтом">Вопрос по работе с сайтом</option>
                            <option value="Ошибка на сайте">Ошибка на сайте</option>
                            <option value="Предложение сотрудничества">Предложение сотрудничества</option>
                            <option value="Другое">Другое</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label for="feedback-message">Сообщение *</label></td>
                    <td><textarea name="message" id="feedback-message" autocomplete="off" required></textarea></td>
                </tr>
            </table>
            <input type="submit" value="Отправить" class="ui-btn btn-blue">
        </div>
        <div class="contacts">
            <h4>Контакты</h4>
            <?=$contacts;?>
        </div>
        <div class="clear"></div>
        <? if($support_send): ?>
            <div class="success">
                <span class="success-text">Спасибо! ваше сообщение принято, менеджер свяжется с вами в ближайшее время.</span>
            </div>
        <? endif; ?>
    </form>
</div>